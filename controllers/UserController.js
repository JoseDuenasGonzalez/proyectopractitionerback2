const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechujmdg12ed/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getUsersV2(req, res) {
  console.log("GET /apitechu/v2/users");

  var httpClient = requestJson.createClient(baseMLABUrl);

  console.log("Client created");

  httpClient.get("user?" + mLabAPIKey,
    function(err, resMlab, body) {
        var response = !err ? body : {
          "msg" : "Error obteniendo usuarios"
        }
        console.log("length " + body.length)
        res.send(response);
    }

  );

}

function getUserByIdV2(req, res) {
  console.log("GET /apitechu/v2/users/:id");

  var id = Number.parseInt(req.params.id);
  console.log("La id del usuario a obtener es " + id);

  var query = "q=" + JSON.stringify({"idUser": id});
  console.log("query es " + query);

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body) {
      /* refactorizamos esto, pero sería lo que tendríamos que coger de ejemplo
      para el resto de funciones por que es lo mas sencillo
      var response = !err ? body[0] : {
        "msg" : "Error obteniendo usuario"
        */
        if (err) {
          var response = {
            "msg" : "Error obteniendo usuario"
          }
          res.status(500);
        } else {
          if (body.length > 0) {
            var response = body[0];
          } else {
            var response = {
              "msg" : "Usuario no encontrado"
            }
            res.status(404);
          }
        }
      res.send(response);
    }
  )
}


function createUserV2(req, res) {
  console.log("POST /apitechu/v2/users");

  console.log(req.body.id);
  console.log(req.body.first_name);
  console.log(req.body.last_name);
  console.log(req.body.email);



  //en segundo lugar vemos el número de usuarios dados de alta para obtener el nuevo id

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  var email = req.body.email;
  console.log("El email del usuario a obtener es " + email);

  var query = "q=" + JSON.stringify({"email": email});
  console.log("query es " + query);

  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err1, resMlab, body1) {
        if (err1) {
          var response = {
            "msg" : "Error obteniendo email"
          }
          res.status(500);
        } else {
          if (body1.length > 0) {
            console.log("email encontrado")
            var response = {
              "msg" : "Usuario ya existe"
            }
            res.status(401);
            res.send(response);
          } else {
            console.log("OK El email no existe")

            httpClient.get("user?" + mLabAPIKey,
              function(err2, resMlab, body2) {
              if (err2) {
                  var response =
                  {
                    "msg" :"error al obtener usuarios"
                  }
                  res.status(500);
                  res.send(response);
                } else {
                    if (body2.length > 0) {
          //la id de usuario sera el numero total de usuarios mas 1
                      newId = body2.length++
                      console.log("newID " + newId)
                      var newUser = {
                        "idUser" : newId,
                        "first_name" : req.body.first_name,
                        "last_name" : req.body.last_name,
                        "email" : req.body.email,
                        "password" : crypt.hash(req.body.password)
                      }
                      //var httpClient = requestJson.createClient(baseMLABUrl);
                      httpClient.post("user?" + mLabAPIKey, newUser,
                        function(err, resMlab, body) {
                          console.log("Usuario creado");
                          var response =
                          {
                            "msg" :"Usuario creado"
                          }
                          res.status(201);
                          res.send(response);
                        }
                      );

                    }
                }
              }

            );

//aqui iria la respuesta de la primera query
          }
        }
//      res.send(response);
    }
  )





}

function deleteUserV1(req, res) {
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("La id del usuario a borrar es " + req.params.id);

  var users = require('../usuarios.json');
  var i = 0;
  var borrado = false;
  var sum = 0;
  var element = 0;
  var value = 0;

  /*Practica borrado con bucle opcion 1 */

  for (var i = 0; i < users.length; i++) {

    console.log("entra en bucle");
    console.log("Id de usuario " + users[i].idUser);

    if (req.params.id == users[i].idUser) {
        console.log("Usuario encontrado");
        console.log("Id de usuario " + users[i].id);
        borrado = true
        break;
}else if
      (i == users.length) {
        console.log("Usuario no encontrado");
      }
    }

  /*Con splice le decimos que quite del valor que nos han pasado
  por parámetro -1 (req.params.id -1) y que quite 1 */
  if (borrado == true) {
  users.splice(i, 1);

  /*llamada a la funcion que escribe el fichero */
  io.writeUserDataToFile(users);

  res.send({"msg":"usuario borrado"});
} else if (borrado == false) {
  res.send({"msg":"no encontrado usuario a borrar"});
 }
}

module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV2 = createUserV2;
module.exports.deleteUserV1 = deleteUserV1;
