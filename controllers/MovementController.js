const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechujmdg12ed/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function getMovementById(req, res) {
  console.log("GET /apitechu/v2/movements/:iban");

  var iban = req.params.iban;
  console.log("La cuenta a obtener sus movimientos es " + iban);

  var query = "q=" + JSON.stringify({"IBAN": iban});
  console.log("query es " + query);

  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("movement?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body) {
        if (err) {
          var response = {
            "msg" : "Error obteniendo usuario"
          }
          res.status(500);
        } else {
          if (body.length > 0) {
            var response = body;
          } else {
            var response = {
              "msg" : "No se han encontrado movimientos"
            }
            res.status(404);
          }
        }
      res.send(response);
    }
  )
}

function createMovement(req, res) {
  console.log("POST /apitechu/v2/movements");

  console.log(req.body.IBAN);
  console.log(req.body.date);
  console.log(req.body.movement_type);
  console.log(req.body.amount);

  var hoy = new Date();
  var dd = hoy.getDate();
  var mm = hoy.getMonth()+1;
  var yyyy = hoy.getFullYear();
  date = yyyy + '-' + mm + '-' + dd;

  console.log("La fecha es " + date)

  var newMovement = {
    "IBAN" : req.body.IBAN,
    "movement_type" : req.body.movement_type,
    "date" : date,
    "amount" : req.body.amount
  }

  console.log(newMovement);


  var httpClient = requestJson.createClient(baseMLABUrl);
  httpClient.post("movement?" + mLabAPIKey, newMovement,
    function(err, resMlab, body) {
      if (err) {
        response = {
          "msg" : "Error alta de movimiento."
        }
        res.status(500);
      } else {
        console.log("Movimiento creado");
        res.status(201).send({"msg":"Movimiento creado"});
        /* Vamos a consultar el balance de la cuenta antes de actualizarla */

        var query = "q=" + JSON.stringify({"IBAN": req.body.IBAN});
        console.log("Query es " + query);

        httpClient = requestJson.createClient(baseMLABUrl);
        httpClient.get("account?" + query + "&" + mLabAPIKey,
          function(err, resMLab, body) {
            console.log(body);

            if (err) {
              response = {
                "msg" : "Error obteniendo cuentas."
              }
              res.status(500);
            } else {
              console.log("Antes de borrar el balance");
              console.log(body[0].balance);
              console.log("TIpo de movimiento: " + req.body.movement_type);
              console.log("Importe: " + req.body.amount);

              query = "q=" + JSON.stringify({"IBAN": req.body.IBAN});
              console.log("Query para actualizar balance " + query);
              var putBody = '{"$unset":{"balance":""}}'
              console.log(putBody);
              httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
                function(errPUT, resMLabPUT, bodyPUT) {
                  console.log("PUT done");
                  if (errPUT) {
                    response = {
                      "msg" : "Error obteniendo cuentas."
                    }
                    res.status(500);

                  } else {
                      console.log("Antes de actualizar el nuevo balance")
                      console.log(body[0].balance);
                      console.log("TIpo de movimiento: " + req.body.movement_type);
                      console.log("Importe: " + req.body.amount);

                      if (req.body.movement_type == "INGRESO") {
                             body[0].balance += req.body.amount;
                           } else {
                             body[0].balance -= req.body.amount;
                           }

                      console.log("Nuevo balance: " + body[0].balance);
                        query = "q=" + JSON.stringify({"IBAN": req.body.IBAN});
                      console.log("Query para actualizar balance " + query);
                      var newBody = '{"$set":{"balance":'+ body[0].balance + '}}'
                      console.log(newBody);
                      httpClient.put("account?" + query + "&" + mLabAPIKey, JSON.parse(newBody),
                        function(errNEW, resMLabNEW, bodyNEW) {
                          console.log("PUT done");
                          if (errNEW) {
                            response = {
                              "msg" : "Error eliminando balance."
                            }
                            res.status(500);

                          }
                        }
                      )




      /* fin del else */
                  }
                }
              )

            }
          }
        );


      }
    }
  );




}


function getRandomNumber(min, max) {
  return Math.random() * (max - min) + min;
}


module.exports.getMovementById = getMovementById;
module.exports.createMovement = createMovement;
