const io = require('../io');
const crypt = require('../crypt');
const requestJson = require('request-json');

const baseMLABUrl = "https://api.mlab.com/api/1/databases/apitechujmdg12ed/collections/";

const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;


function loginV2(req, res) {
 console.log("POST /apitechu/v2/login");

 var query = "q=" + JSON.stringify({"email": req.body.email});
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLABUrl);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {

     var isPasswordcorrect =
       crypt.checkPassword(req.body.password, body[0].password);
     console.log("Password correct is " + isPasswordcorrect);

     if (!isPasswordcorrect) {
       var response = {
         "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
       }
       res.status(401);
       res.send(response);
     } else {
       console.log("Got a user with that email and password, logging in");
       var id = Number.parseInt(body[0].idUser);
       query = "q=" + JSON.stringify({"idUser": id});
       console.log("Query for put is " + query);
       var putBody = '{"$set":{"logged":true}}';
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario logado con éxito",
             "idUser" : body[0].idUser
           }
           res.send(response);
         }
       )
     }
   }
 );
}

function logoutV2(req, res) {
 console.log("POST /apitechu/v2/logout/:idUser");
 var id = Number.parseInt(req.params.id);
 var query = "q=" + JSON.stringify({"idUser": id});
 console.log("query es " + query);
 httpClient = requestJson.createClient(baseMLABUrl);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       console.log("Got a user with that id, logging out");
       id = Number.parseInt(body[0].idUser);
       query = "q=" + JSON.stringify({"idUser": id});
       console.log("Query for put is " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "idUser" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}



module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
